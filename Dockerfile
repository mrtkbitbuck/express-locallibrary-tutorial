FROM node:9

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

#you need to publish on these ports
EXPOSE 3000:3000

CMD ["npm", "start"]